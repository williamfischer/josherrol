import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NgForm, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { WeatherPage } from '../weather/weather'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  twitter: any;
  TwitterResults: any;
  otherResults: any;
  SearchParams: string;
  GoogleSearchResults: any;

  constructor(public navCtrl: NavController, private http: HttpClient) {

  }

  runSearch(SearchParams){
    console.log(SearchParams.viewModel);

    interface UserResponse {
          query: any;
          pages: any;
    }

    this.http.get < UserResponse > ('https://www.googleapis.com/customsearch/v1?key=AIzaSyC5fd5sFx7g6XHHLaLl8N7E7uhOm2qD9-g&cx=017576662512468239146:omuauf_lfve&q=' + SearchParams.viewModel).subscribe(
          data => {
              console.log(data)
              this.otherResults = data;
              this.otherResults = Array.of(this.otherResults);
          },
          err => console.log("Error: " + JSON.stringify(err)), () => {
              Object.keys(this.otherResults).forEach(key => {
                  this.GoogleSearchResults = this.otherResults[key].items
              });
          }
      );
  }

}
