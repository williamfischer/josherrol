import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from "../home/home"
import { WeatherPage } from "../weather/weather"
import { SettingsPage } from "../settings/settings"

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1root = HomePage;
  tab2root = WeatherPage;
  tab3root = SettingsPage;
}
